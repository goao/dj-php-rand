#!/usr/bin/php
<?php
/**
 * author: MC LOC <mcl.caseiro@gmail.com>
 * motive: need simple random music player
 * subject: void;
 * weapon: void;
 * exceptions: NEED TO BE PROVEN FROM NOW ON; 
 */


$dj = new DJ();
$dj->main();

class DJ {
	
// 	const LIBRARY ='/home/bradoc/Music/015/OverNight';
	const LIBRARY ='/home/mcloc/Music/';
	public $list = array();
	private $start;
	private $total;
	private $dj_dir;
	
	
	public function __construct(){
		$this->start = time();
		
		
		$this->dj_dir = $_SERVER['HOME'].'/.dj/';
		if(!file_exists($this->dj_dir)){
			exec('mkdir '.$this->dj_dir);
		}
		
		if(file_exists($this->dj_dir.'library')){
			echo "Reading library, hold on...\n";
			$this->list = file_get_contents($this->dj_dir.'library');
			$this->list = explode("\n",$this->list);
		}
		
		if(count($this->list) > 0)
			$sync = false;
		else 
			$this->loadLibray();
		
		$this->total = count($this->list);
		
		
		
	}
	
	public function loadLibray(){
		echo "Loading library, hold on...\n";
		$directories = $this->expandDirectories(self::LIBRARY);
		
		foreach($directories as $d){
			if ($handle = opendir($d)) {
				while (false !== ($entry = readdir($handle))) {
					if ($entry == "." || $entry == "..")
						continue;
		
					if(!strstr($entry, '.mp3'))
						continue;
					
					$file = escapeshellarg($d).'/'.escapeshellarg($entry);
						
					$this->list[] = $file;
					file_put_contents($this->dj_dir.'library', $file."\n", FILE_APPEND);
				}
				closedir($handle);
			}
		}
	}

	/**
	 *   press "q" to jump to the other song;
	 *   press "0" to increase the volume;
	 *   press "9" to decrese the volume;
	 *   press ctrl-c to exit auto dj;
	 *   press "space" to pause/resume;
	 *
	 */
	public function main() {
		echo time()-$this->start.' seconds to load '.$this->total.' in library';
		while(true){
			$track = rand(0, count($this->list)-1);
			echo "playing ".$this->list[$track]."\n";
			
			try{
				shell_exec(" mplayer ".$this->list[$track].'  > /dev/null 2>&1');
			} catch (Exception $e){
				echo "show me the Exception your excelency...\n";
				exit -1;
			}
// 			$return = shell_exec('whoami');
// 			echo $return;
		}
	}

	
	//TKS to the Stackflow fellow who contributed wuth this method...
	/**
	 * Lambda php implementation TKS
	 * @param unknown $base_dir
	 * @return multitype:string
	 */
	public function expandDirectories($base_dir) {
		$directories = array();
		foreach(scandir($base_dir) as $file) {
			if($file == '.' || $file == '..') continue;
			$dir = $base_dir.DIRECTORY_SEPARATOR.$file;
			if(is_dir($dir)) {
				$directories []= $dir;
				$directories = array_merge($directories, $this->expandDirectories($dir));
			}
		}
		return $directories;
	}

}
?>